package br.com.galgo.testes.recursos_comuns.teste;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaCarteiras;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class TesteConsultaCarteiras extends Teste {

	private final String PASTA_TESTE = "Fundos";

	private TelaLogin telaLogin;
	private Ambiente ambiente;

	@Before
	public void setUp() throws Exception {
		configurar();
	}

	public void configurar() {
		ambiente = TesteUtils.configurarTeste(Ambiente.HOMOLOGACAO,
				PASTA_TESTE);
		configurar(ConstantesTestes.PATH_ARQUIVO_UPLOAD_COM_ERRO, null,
				ambiente);
	}

	public void configurar(String pathUpload, Operacao operacao,
			Ambiente ambiente) {
		TelaGalgo.abrirBrowser(ambiente.getUrl());
		telaLogin = new TelaLogin();

		this.ambiente = ambiente;
	}

	@Test
	public void testeConsultaCarteiras() throws ErroAplicacao {
		this.setNomeTeste("testeConsultaCarteiras.png");
		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.ADMINISTRADOR);
		Usuario usuario = new Usuario(usuarioConfig);

		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaConsultaCarteiras telaConsulta = (TelaConsultaCarteiras) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_CARTEIRAS_ADMINISTRADAS);

		telaConsulta.clicarConfirmar();
		telaConsulta.verificarItemEncontrado();
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste(this.getNomeTeste());
	}

}
