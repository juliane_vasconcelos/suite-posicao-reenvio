package br.com.galgo.testes.recursos_comuns.teste;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.data.FormatoData;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroLog;
import br.com.galgo.testes.recursos_comuns.enumerador.filtro.camposTela.CampoFiltroUsuario;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaLog;
import br.com.galgo.testes.recursos_comuns.utils.DataUtils;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class TesteConsultaLog extends Teste {

	private final String PASTA_TESTE = "Log";
	private TelaLogin telaLogin;
	private Ambiente ambiente;

	@Before
	public void setUp() throws Exception {
		ambiente = TesteUtils.configurarTeste(Ambiente.PRODUCAO, PASTA_TESTE);
		TelaGalgo.abrirBrowser(ambiente.getUrl());
		telaLogin = new TelaLogin();
	}

	@Test
	public void testeConsultaDeLog() throws ErroAplicacao {
		this.setNomeTeste("testeConsultaDeLog.png");

		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.ADM_SISTEMA);
		Usuario usuario = new Usuario(usuarioConfig);

		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaConsultaLog telaConsultaDeLog = (TelaConsultaLog) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_DE_LOG);

		Date dataFiltro = DataUtils.subtrairMinutos(
				ConfiguracaoSistema.MINUTOS_ANTES_LOGIN, new Date());
		String dataInicial = FormatoData.DD_MM_YYYY.formata(dataFiltro);
		String horaInicial = FormatoData.HH_MM.formata(dataFiltro);

		telaConsultaDeLog.incluirFiltro(CampoFiltroLog.INPUT_DATA_INICIAL,
				dataInicial);
		telaConsultaDeLog.incluirFiltro(CampoFiltroLog.INPUT_HORA_INICIAL,
				horaInicial);

		telaConsultaDeLog.incluirFiltroBuscaUsuario(
				CampoFiltroLog.BUSCA_USUARIO,
				CampoFiltroUsuario.INPUT_LOGIN_USUARIO,
				usuarioConfig.getLogin());

		telaConsultaDeLog.clicarBotaoConfirmar();
		telaConsultaDeLog.verificarItemEncontrado();
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste(this.getNomeTeste());
	}

}
