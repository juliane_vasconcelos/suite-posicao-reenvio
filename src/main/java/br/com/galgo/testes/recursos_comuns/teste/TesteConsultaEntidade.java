package br.com.galgo.testes.recursos_comuns.teste;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.testes.recursos_comuns.enumerador.Entidade;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaEntidade;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class TesteConsultaEntidade extends Teste {

	private final String PASTA_TESTE = "Entidade";
	private Ambiente ambiente;
	private TelaLogin telaLogin;

	@Before
	public void setUp() throws Exception {
		ambiente = TesteUtils
				.configurarTeste(Ambiente.HOMOLOGACAO, PASTA_TESTE);
		TelaGalgo.abrirBrowser(ambiente.getUrl());
		telaLogin = new TelaLogin();
	}

	@Test
	public void testeConsultaTodasEntidadePJ() throws ErroAplicacao {
		this.setNomeTeste("testeConsultaTodasEntidadePJ.png");
		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.ADM_SISTEMA);
		Usuario usuario = new Usuario(usuarioConfig);

		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaConsultaEntidade telaEntidade = (TelaConsultaEntidade) telaHome
				.acessarSubMenu(SubMenu.CADASTRO_ENTIDADES);
		telaEntidade.consultarTodasEntidadesPJ(telaHome);
	}

	@Test
	public void testeConsultaEntidadeAutoreguladora() throws ErroAplicacao {
		this.setNomeTeste("testeConsultaEntidadeAutoreguladora.png");

		final UsuarioConfig usuarioConfig = UsuarioConfig.fromCategoria(
				ambiente, Categoria.USUARIO_FINAL, Papel.ADM_SISTEMA);
		Usuario usuario = new Usuario(usuarioConfig);

		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaConsultaEntidade telaEntidade = (TelaConsultaEntidade) telaHome
				.acessarSubMenu(SubMenu.CADASTRO_ENTIDADES);
		telaEntidade.consultarEntidadePJ(Entidade.fromPapel(ambiente,
				Papel.AUTO_REGULADOR));
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste(this.getNomeTeste());
	}
}
