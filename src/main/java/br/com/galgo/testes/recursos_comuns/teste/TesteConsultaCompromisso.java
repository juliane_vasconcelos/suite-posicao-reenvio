package br.com.galgo.testes.recursos_comuns.teste;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.compromisso.TelaPainelCompromissos;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class TesteConsultaCompromisso extends Teste {

	private final String PASTA_TESTE = "Compromisso";
	private TelaLogin telaLogin;
	private Ambiente ambiente;
	private UsuarioConfig usuarioConfig;

	@Before
	public void setUp() throws Exception {
		ambiente = TesteUtils.configurarTeste(Ambiente.HOMOLOGACAO, PASTA_TESTE);
		TelaGalgo.abrirBrowser(ambiente.getUrl());
		telaLogin = new TelaLogin();

		if (usuarioConfig == null) {
			usuarioConfig = UsuarioConfig.fromCategoria(ambiente,
					Categoria.USUARIO_FINAL, Papel.ADM_SISTEMA);
		}
	}

	@Test
	public void testeConsultaPainelCompromissos() throws ErroAplicacao {
		this.setNomeTeste("testeConsultaPainelCompromissos.png");

		int tentativas = ConfiguracaoSistema.MAX_RETENTATIVA;
		boolean abriuPainel = testePainelCompromisso();

		while (!abriuPainel && haTentativas(tentativas)) {
			tentativas--;
			TelaGalgo.reiniciarBrowser(ambiente.getUrl());
			abriuPainel = testePainelCompromisso();
		}

		Assert.assertTrue("Erro ao acessar o painel de compromissos!",
				abriuPainel);
	}

	private boolean haTentativas(int tentativas) {
		return tentativas > 0;
	}

	private boolean testePainelCompromisso() throws ErroAplicacao {

		Usuario usuario = new Usuario(usuarioConfig);

		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaPainelCompromissos telaPainelCompromissos = (TelaPainelCompromissos) telaHome
				.acessarSubMenu(SubMenu.PAINEL_COMPROMISSO);

		if (telaPainelCompromissos.abriuPainelCompromissos()) {
			if (telaPainelCompromissos.pegarQtdDeEventosPendentes() > 0) {
				return true;
			}
		}
		return false;
	}

	public void configura(Ambiente ambiente, UsuarioConfig usuarioConfig) {
		this.ambiente = ambiente;
		this.usuarioConfig = usuarioConfig;
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste(this.getNomeTeste());
	}

}
